package io.sages;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

public class IOMain {

    public static void main(String[] args) {
        System.out.println("Let's ???");

        File file = getNIOFile();

        // try-with-resources
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(file, true))) {

            bw.write(LocalTime.now().format(DateTimeFormatter.ISO_TIME) + "\n");

        } catch (IOException e) {
            throw new RuntimeException(e);
        } /*finally {
            try {
                if(bw!=null) {
                    bw.close();
                }
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }*/


        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            /*String line;
            while((line=br.readLine())!=null){
                LocalTime time = LocalTime.parse(line, DateTimeFormatter.ISO_TIME);
                System.out.println("time: " + time);
            }*/

            br.lines().forEach(line -> {
                LocalTime time = LocalTime.parse(line, DateTimeFormatter.ISO_TIME);
                System.out.println("time: " + time);
            });

        } catch (IOException e) {
            throw new RuntimeException(e);
        }


    }

    private static File getNIOFile() {

        try {
            Path dir = Paths.get("mydir");
            if (!Files.exists(dir)) {
                System.out.println("creating missing directory");
                Files.createDirectories(dir);
            } else {
                System.out.println("directory is present");
            }

            Path path = Paths.get("mydir", "myfile.txt");
            if (!Files.exists(path)) {
                System.out.println("creating missing file");
                Files.createFile(path);
            } else {
                System.out.println("file is present");
            }
            return path.toFile();
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }


    private static File getIOFile() {
        File dir = new File("mydir");

        if (!dir.exists()) {
            System.out.println("creating missing directory");
            dir.mkdirs();
        } else {
            System.out.println("directory is present");
        }

        File file = new File(dir, "myfile.txt");
        if (!file.exists()) {
            System.out.println("creating missing file");
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            System.out.println("file is present");
        }

        return file;
    }

}
