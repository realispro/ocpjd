package io.sages.zoo;

import java.io.Serial;
import java.io.Serializable;

public class Shark extends Fish implements Serializable {

    @Serial
    private static final long serialVersionUID = 3754529233450525899L;


    private transient String foo = "bar";

    public Shark(String name, int size) {
        super(name, size);
    }

    public Shark(){
    }
}
