package io.sages.zoo;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class ZooStreamMain {


    public static void main(String[] args) {
        System.out.println("Let's stream zoo!");

        Eagle bielik = new Eagle("Bielik", 15);
        Kiwi jack = new Kiwi("Jack", 5);
        Shark willy = new Shark("Willy", 300);
        Shark willyBis = new Shark("Willy", 300);
        Shark joe = new Shark("Joe", 150);

        List<Animal> animals = new LinkedList<>();
        animals.add(bielik);
        animals.add(jack);
        animals.add(willy);
        animals.add(willyBis);
        animals.add(joe);


        Stream<Animal> stream = animals.parallelStream();

        if(!stream.isParallel()){
            stream = stream.parallel();
        }


        var result =
                stream
                // intermediate operations
                        .sorted((a1,a2)->a1.getName().compareTo(a2.getName()))
                        .distinct()
                        .filter( a -> !a.getName().equals("Jack"))
                        .peek(a->a.name=a.name+a.name)
                        .map(a->a.getSize())

                // terminal operation
                        .findFirst();
                        //.anyMatch(size->size>250);
                        //.toList(); //collect(Collectors.toList());
                        //.count();


        System.out.println("result = " + result.orElseThrow( ()->new IllegalArgumentException("foo") ));


        IntStream intStream = IntStream.of(4, 7, 2, 4, 9);
        var average = intStream.average();
        System.out.println("average: " + average.getAsDouble());

        Stream<Integer> integerStream = Stream.iterate( 0, i -> i+1 );
                // Stream.generate( () -> 13);
        integerStream
                .limit(13)
                .forEach(i-> System.out.println(i));

    }
}
