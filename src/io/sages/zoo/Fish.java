package io.sages.zoo;

import java.io.Serializable;

public abstract class Fish extends Animal implements Serializable {

    public Fish(String name, int size) {
        super(name, size);
    }


    public Fish(){
    }

    @Override
    public void move() {
        System.out.println(name + " is swimming");
    }
}
