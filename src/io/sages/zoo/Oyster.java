package io.sages.zoo;

public class Oyster extends Fish{

    private static String staticResource = "static";

    private String resource = "instance";

    public Oyster(String name, int size) {
        super(name, size);
    }



    public static class Pearl{

        private String mass = "sand";

        public String grow(){
            mass = "[" + mass + "]" + staticResource;
            return mass;
        }

    }



}
