package io.sages.zoo;

import java.io.Serializable;
import java.util.Objects;

public abstract class Animal implements Comparable<Animal>, Serializable {

    protected String name;
    protected int size;

    public Animal(String name, int size) {
        this.name = name;
        this.size = size;
    }


    public Animal(){}


    public void eat(String food){
        System.out.println(name + " is eating " + food);
    }

    public abstract void move();


    public String getName() {
        return name;
    }

    public int getSize() {
        return size;
    }

    @Override
    public int compareTo(Animal a) {
        return a.size - this.size;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Animal animal = (Animal) o;

        if (size != animal.size) return false;
        return Objects.equals(name, animal.name);
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + size;
        return result;
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + "{" +
                "name='" + name + '\'' +
                ", size=" + size +
                '}';
    }
}
