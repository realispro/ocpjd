package io.sages.zoo;

public abstract class Bird extends Animal{

    public Bird(String name, int size) {
        super(name, size);
    }

    @Override
    public void move() {
        System.out.println(name + " is flying");
    }
}
