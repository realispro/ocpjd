package io.sages.zoo;

import java.io.File;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.sql.*;
import java.util.*;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

public class ZooMain {

    public static void main(String[] args) {
        System.out.println("Let's visit ZOO!");

        Eagle bielik = new Eagle("Bielik", 15);
        Kiwi jack = new Kiwi("Jack", 5);
        Shark willy = new Shark("Willy", 300);
        Shark willyBis = new Shark("Willy", 300);
        Shark joe = new Shark("Joe", 150);

        //Set<Animal> animals = new HashSet<>();
        List<Animal> animals = new LinkedList<>();
                //new ArrayList<>();
        animals.add(bielik);
        animals.add(jack);
        animals.add(willy);
        animals.add(willyBis);
        animals.add(0, joe);

        System.out.println("animals count " + animals.size());

        //animals.remove(willy);

        /*for(var animal : animals){
            if(animal.name.equals("Willy")){
                animals.remove(animal);
            }
        }*/

        if(animals.contains(new Shark("Willy", 300))){
            System.out.println("Willy is still here!");
        }



        // ************** filtering *************


        Predicate<Animal> predicate = a -> a.getName().equals("Willy");

                /*new Predicate<Animal>() {
            @Override
            public boolean test(Animal animal) {
                return animal.getName().equals("Willy");
            }
        };*/
        animals.removeIf(a -> a.getName().equals("Willy"));

        /*Iterator<Animal> itr = animals.iterator();
        while(itr.hasNext()){
            Animal animal = itr.next();
            if(animal.name.equals("Willy")){
                itr.remove();
            }
        }*/


        // *************** sorting *******************
        Comparator<Animal> comparator = (a1, a2) -> a1.getSize()-a2.getSize();

                /*new Comparator<Animal>() {
            @Override
            public int compare(Animal a1, Animal a2) {
                return a1.getSize()-a2.getSize();
            }
        };*/
                //new AnimalComparator();
        Collections.sort(animals, (a1, a2) -> a1.getSize()-a2.getSize());


        // *************** visiting **************
        Consumer<Animal> consumer =  a -> System.out.println("[lambda consumer] animal = " + a);
                /*new Consumer<Animal>() {
            @Override
            public void accept(Animal animal) {
                System.out.println("[consumer] animal = " + animal);
            }
        };*/
        animals.forEach(a -> System.out.println("[lambda consumer] animal = " + a));

       /* for( var animal : animals){
            System.out.println("animal= " + animal);
        }*/




        Map<Integer, Animal> habitats = new HashMap<>();

        habitats.put(1, willy);
        habitats.put(2, jack);
        habitats.put(3, joe);
        habitats.put(4, bielik);

        habitats.put(3, willy);

        for( Map.Entry<Integer, Animal> entry : habitats.entrySet()){
            System.out.println("key:" + entry.getKey() + ", value:" + entry.getValue());
        }


        Supplier<Animal> supplier = () -> habitats.get(3);
        Animal animal = supplier.get();

        Function<Animal, String> function = a -> a.getName();

        String name = function.apply(animal);



        // ***** inner class

        Oyster oyster = new Oyster("Billy", 1);

        //Oyster.Pearl pearl = oyster.new Pearl();
        Oyster.Pearl pearl = new Oyster.Pearl();


        System.out.println("pearl:" + pearl.grow());
        System.out.println("pearl:" + pearl.grow());
        System.out.println("pearl:" + pearl.grow());



        // ********* serialization




        File file = new File("mydir\\animal.lab");

        /*try(ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file))){
            Animal deserializedAnimal = (Animal) ois.readObject();
            System.out.println("deserializedAnimal = " + deserializedAnimal);
        } catch (IOException e) {
            throw new RuntimeException(e);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }*/



        /*try(ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(file))){
            oos.writeObject(animal);
        }  catch (IOException e) {
            throw new RuntimeException(e);
        }*/


        // *************  database ******************


     /*   try(Connection connection = getConnection()){
            connection.setAutoCommit(false);

            System.out.println("connected. " + connection.getMetaData().getDatabaseProductVersion());

            final String INSERT_SQL = "INSERT INTO ANIMAL(NAME, SIZE, TYPE) VALUES (?,?,?)";
            PreparedStatement stmt = connection.prepareStatement(INSERT_SQL);

            //stmt.setString(1, animal.getName());
            stmt.setObject(1, animal.getName(), Types.VARCHAR);
            stmt.setInt(2, animal.getSize());
            stmt.setString(3, animal.getClass().getName());

            stmt.addBatch();

            //int count = stmt.executeUpdate();
            //System.out.println("rows affected = " + count);

            stmt.setString(1, animal.getName() + animal.getName());
            stmt.setInt(2, animal.getSize()*2);
            //stmt.setString(3, animal.getClass().getName());

            stmt.addBatch();

            int[] count = stmt.executeBatch();
            System.out.println("rows affected = " + Arrays.toString(count));

            connection.commit();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }*/


        try(Connection connection = getConnection()){

            final String SELECT_ANIMAL = "SELECT ID, NAME, SIZE, TYPE FROM ANIMAL";


            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery(SELECT_ANIMAL);

            while(rs.next()){

                String animalName = rs.getString(2);
                int animalSize = rs.getInt("SIZE");
                String animalType = rs.getString("TYPE");

                //System.out.println("name: " + animalName + ", size: " + animalSize + ", type: " + animalType);

                Class animalClass = Class.forName(animalType);

                Constructor constructor = animalClass.getConstructor(String.class, int.class);
                Animal someAnimal = (Animal) constructor.newInstance(animalName, animalSize);
                System.out.println("someAnimal = " + someAnimal);

            }


        } catch (SQLException e) {
            throw new RuntimeException(e);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        } catch (NoSuchMethodException e) {
            throw new RuntimeException(e);
        } catch (InvocationTargetException e) {
            throw new RuntimeException(e);
        } catch (InstantiationException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }


    }


    private static Connection getConnection(){

        String driverClass = "com.mysql.cj.jdbc.Driver";
        String url = "jdbc:mysql://localhost:3306/ocpjd";
        String user = "root";
        String password = "mysql";

        try {
            //Class.forName(driverClass);
            return DriverManager.getConnection(url, user, password);

        } catch ( SQLException e) {
            e.printStackTrace();
            return null;
        }
    }
}
