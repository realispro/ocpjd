package io.sages.zoo;

public class Aquarium<T extends Object> {

    private T o;

    public void add(T o){
        this.o = o;
    }

    public T get(){
        return this.o;
    }



    public static <X> Aquarium<X> of(X o){
        Aquarium<X> aquarium = new Aquarium<>();
        aquarium.add(o);
        return aquarium;
    }

}
