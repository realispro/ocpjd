package io.sages.zoo;

public class GenericMain {

    public static void main(String[] args) {
        System.out.println("Let's generalize!");


        Aquarium<Oyster> aquarium = Aquarium.of(new Oyster("Nemo", 1));
                /*new FishAquarium<>();
        aquarium.add(new Oyster("Nemo", 1));*/

        Fish fish = aquarium.get();

        System.out.println("fish = " + fish);




    }
}
