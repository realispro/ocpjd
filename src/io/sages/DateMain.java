package io.sages;

import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.ResourceBundle;

public class DateMain {

    public static void main(String[] args) {

        Date date = new Date();

        Calendar calendar = Calendar.getInstance();

        //calendar.setTime(date);
        calendar.set(2022, Calendar.NOVEMBER, 16, 12, 31);
        //calendar.add(Calendar.MONTH, 2);

        date = calendar.getTime();


        Locale locale = Locale.ITALIAN;
                // new Locale("pl", "PL");
                //new Locale("hu", "HU");
        DateFormat format = new SimpleDateFormat("w D dd*MM*yyyy hh*mm*ss*SSS G", locale);
                //DateFormat.getDateTimeInstance(DateFormat.FULL, DateFormat.FULL, locale);
        String timestamp = format.format(date);
        System.out.println("timestamp = " + timestamp);

        // *********  java.time **************

        Locale inLocale = //Locale.US;
                // new Locale("pl", "PL");
                Locale.ENGLISH;

        // messages_pl_PL.properties
        // messages_pl.properties
        // messages.properties

        ResourceBundle bundle = ResourceBundle.getBundle("messages", inLocale);

        LocalDate ld = LocalDate.now();
        LocalTime lt = LocalTime.of(16, 30, 0).minusHours(4);
        LocalDateTime ldt = LocalDateTime.of(ld, lt);

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("w D dd*MM*yyyy hh*mm*ss*SSS G", locale);

        timestamp = ldt.format(formatter);
                //formatter.format(ld);
        System.out.println("java.time: " + bundle.getString("lab.timestamp") + ":" + timestamp);

        ZonedDateTime warsawDateTime = ZonedDateTime.of(ldt, ZoneId.of("Europe/Warsaw"));

        ZonedDateTime landingDateTime = warsawDateTime.plusHours(8).withZoneSameInstant(ZoneId.of("Asia/Singapore"));

        System.out.println( bundle.getString("lab.landing.time") + ": " + landingDateTime);
        
        Duration duration = Duration.between(warsawDateTime, landingDateTime);
        System.out.println( bundle.getString("lab.duration") + ": " + duration);

        Period p = Period.ofDays(1).plusYears(2);
        System.out.println("p = " + p);

        NumberFormat nf = NumberFormat.getCurrencyInstance(inLocale);
        nf.setMinimumFractionDigits(2);
        nf.setMaximumFractionDigits(4);
        System.out.println("nf: " + nf.format(123456789.12345f));


        WeekDay weekDay = null;//getWeekDay();
        WeekDay anotherWeekDay = getAnotherWeekDay();

        System.out.println(
                "weekday: " + weekDay.name()
                        + " ordinal: " + weekDay.ordinal()
                        + " weekend: " + weekDay.isWeekend());

        weekDay = weekDay.MONDAY;

        if(weekDay == anotherWeekDay){
            System.out.println("same week day!");
        } else {
            System.out.println("another week day!");
        }

    }


    public static WeekDay getWeekDay(){
        return WeekDay.FRIDAY;
    }

    public static WeekDay getAnotherWeekDay(){
        return WeekDay.FRIDAY;
    }



}
