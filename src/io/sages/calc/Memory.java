package io.sages.calc;

public interface Memory {

    int tmp = 123;

    double getValue();

    void setValue(double value);

}
