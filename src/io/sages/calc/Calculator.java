package io.sages.calc;

import io.sages.calc.memory.ArrayMemory;

public abstract class Calculator /*extends Object*/ {

    static {
        System.out.println("static block");
    }

    //protected double result = 0 ;
    protected Memory memory = new ArrayMemory();

    {
        System.out.println("initialization block");
    }

    public Calculator(double result){
        super();
        // initialization
        System.out.println("parametrized calc constructor: " + result);
        this.memory.setValue(result);
    }

    public Calculator(){
        this(0);
        System.out.println("default calc constructor");
    }

    public abstract void display();

    public double add(double operand){
        memory.setValue(memory.getValue()+operand);
        return memory.getValue();
    }

    public double subtract(double operand){
        memory.setValue(memory.getValue()-operand);
        return memory.getValue();
    }

    public double multiply(double operand){
        memory.setValue(memory.getValue()*operand);
        return memory.getValue();
    }

    public double divide(double operand) throws CalculatorException {
        memory.setValue(memory.getValue()/operand);
        return memory.getValue();
    }


    public double getResult() {
        return memory.getValue();
    }

    public static int multiply(int operand, int... operands){
        // 3, {4, 5, 6}

        for(int i=0; i<operands.length; i++){
            operand = operand * operands[i];
        }
        return operand;
    }

    @Override
    public String toString() {
        return "Calculator{" +
                "memory=" + memory +
                '}';
    }
}
