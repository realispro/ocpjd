package io.sages.calc;

public class CalculatorMain {

    public static void main(String[] args) {
        System.out.println("Let's calculate!");

        Calculator c1 = new ScientificCalculator(2);
        Calculator c2 = new ScientificCalculator();

        try {
            c1.add(2);
            c1.multiply(3);
            c1.divide(6);
            c1.subtract(2);

            c1.display();
            System.out.println("c1: " + c1.getResult());

            c2.toString();
            c2.add(12);
            ((ScientificCalculator) c2).power(4);
            c2.divide(0);
        } catch (CalculatorException | NumberFormatException | NullPointerException e) {
            System.out.println("exception: " + e.getMessage());
            return;
        } catch (Exception e){
            System.out.println("exception: " + e.getMessage());
        } finally {
            System.out.println("in finally block ");
        }

        c2.display();
        System.out.println("c2: " + c2.getResult());
    }
}
