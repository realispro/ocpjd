package io.sages.calc;

public final class ScientificCalculator extends Calculator {

    {
        System.out.println("initialization block in subclass");
    }

    public ScientificCalculator(){
        super();
        // initialization
    }

    @Override
    public void display() {
        System.out.println("Display: " + this);
    }

    public ScientificCalculator(double result){
        super(result);
    }

    public double power(int operand){

        memory.setValue(Math.pow(memory.getValue(), operand));
        return memory.getValue();
    }

    @Override
    public double divide(double operand) throws CalculatorException {
        if(operand==0){
            throw new CalculatorException("pamietaj cholero nie dziel przez zero");
        }
        return super.divide(operand);
    }

}
