package io.sages.calc.memory;

import io.sages.calc.Memory;

public class ArrayMemory implements Memory {


    private double[] values = new double[5];

    private int index = 0;


    @Override
    public double getValue() {
        return values[index];
    }

    @Override
    public void setValue(double value) {

        // [0, 1, 2, 3, 4] + 5
        // [1, 2, 3, 4, 5]

        if((index+1)==values.length){
            System.arraycopy(values, 1, values, 0, values.length-1);
        } else {
            index++;
        }
        values[index] = value;
    }

    @Override
    public String toString() {


        StringBuilder builder = new StringBuilder("[");
        for(var v : values){
            builder.insert(builder.length(), v).append(",");
        }

        String array = builder.append("]").toString();

        /*for(var v : values){
            array = array + v + ",";
        }
        array+="]";*/

        return "ArrayMemory{" +
                "values=" + array +
                ", index=" + index +
                '}';
    }
}
