package io.sages.calc;

import java.io.Serializable;

public interface HistoryMemory extends Memory, Serializable {

    double[] getHistory();

}
