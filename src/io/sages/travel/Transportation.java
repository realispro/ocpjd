package io.sages.travel;

@FunctionalInterface
public interface Transportation {

    public static final int field = 8;

    // Java 8
    public static String getDescription(){
        return "transportation provides transport function " + getSuffix();
    }

    // Java 9
    private static String getSuffix(){
        return " some suffix";
    }

    public void transport(String passenger);

    // Java 8
    public default int getSpeed(){
        return Integer.MAX_VALUE*getFactor();
    }

    // Java 9
    private int getFactor(){
        return 2;
    }

}
