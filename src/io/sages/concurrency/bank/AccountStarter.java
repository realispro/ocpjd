package io.sages.concurrency.bank;

import io.sages.concurrency.ThreadNamePrefixPrintStream;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class AccountStarter {

    public static void main(String[] args) {

        System.setOut(new ThreadNamePrefixPrintStream(System.out));

        ExecutorService es = Executors.newFixedThreadPool(2);

        Account a = new Account(1000);

        DepositTask depositTask = new DepositTask(a);
        WithdrawTask withdrawTask = new WithdrawTask(a);

        es.execute( () -> depositTask.deposit());
        es.execute( () -> withdrawTask.withdraw());

        es.shutdown();

        System.out.println("account: " + a.getAmount());

    }
}
