package io.sages.concurrency.bank;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Account {

    private int amount;

    private Lock lock = new ReentrantLock();

    public Account(int amount) {
        this.amount = amount;
    }

    public void deposit(int value){
        lock.lock();
        amount += value; // amount = amount + value
        lock.unlock();
    }

    public  void withdraw(int value){
        lock.lock();
        amount -= value; // amount = amount - value
        lock.unlock();
    }

    public int getAmount() {
        return amount;
    }
}
