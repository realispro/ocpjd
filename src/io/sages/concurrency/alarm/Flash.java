package io.sages.concurrency.alarm;

public class Flash extends Alarm {
    @Override
    protected void specificAlarm() {
        for(int i=0; i<15; i++){
            System.out.println("FLASH");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
