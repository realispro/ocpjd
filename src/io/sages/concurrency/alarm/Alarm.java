package io.sages.concurrency.alarm;

import java.util.Random;
import java.util.concurrent.Callable;

public abstract class Alarm implements Runnable, Callable<Boolean> {

    public void alarm(){

        specificAlarm();
    }

    protected abstract void specificAlarm();

    @Override
    public void run() {
        alarm();
    }

    @Override
    public Boolean call() throws Exception {
        alarm();
        return new Random().nextBoolean();
    }
}
