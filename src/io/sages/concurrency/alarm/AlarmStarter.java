package io.sages.concurrency.alarm;



import io.sages.concurrency.ThreadNamePrefixPrintStream;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

public class AlarmStarter {

    public static void main(String[] args) {

        System.setOut(new ThreadNamePrefixPrintStream(System.out));

        ExecutorService es = Executors.newFixedThreadPool(2);

        List<Alarm> alarms = new ArrayList<>();
        alarms.add(new Beeper());
        alarms.add(new Flash());

        List<Future<Boolean>> futures = new ArrayList<>();
        alarms.forEach(a->futures.add(es.submit((Callable<Boolean>)a)));

        es.shutdown();
        
        
        futures.forEach(f-> {
            try {
                boolean b = f.get();
                System.out.println("b = " + b);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            } catch (ExecutionException e) {
                throw new RuntimeException(e);
            }
        });

        System.out.println("done.");
}
}
