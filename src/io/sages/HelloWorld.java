package io.sages;

//import io.sages.calc.Calculator;

import java.util.Arrays;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static io.sages.calc.Calculator.multiply;

public class HelloWorld {

    public static void main(String[] args) {
        System.out.println("Let's learn!");

        var age = 12812346;
        System.out.println("age = " + age);

        float price = 12.34F;
        System.out.println("price = " + price);

        boolean weekend = false;
        System.out.println("weekend = " + weekend);

        char c = '\t';//65;
        System.out.println("c = " + (short) c);


        Integer i1 = 128;
        //new Integer(1);
        Integer i2 = 128;
        //new Integer(1);
        System.out.println(i1 == i2);
        System.out.println(i1.equals(i2));

        int[] values = /*new int[]*/{1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        int[] valuesCopy = new int[10];
        System.arraycopy(values, 0, valuesCopy, 0, values.length);

        values[0] = 1;
        values[1] = 2;

        System.out.println("Arrays.toString(values) = " + Arrays.toString(values));

        for (int i = 0; i < values.length; i++) {
            System.out.println("values[" + i + "]=" + values[i]);
        }

        for (int value : values) {
            System.out.println("value = " + value);
        }


        int size = 15;
        int[][] result = new int[size][size];


        outer:
        for (int x = 1; x <= size; x++) {
            for (int y = 1; y <= size; y++) {
                if (y == 13) {
                    break outer;
                }
                result[x - 1][y - 1] = multiply(x, y);
            }
        }

        for (var row : result) {
            for (var value : row) {
                System.out.print(value + "\t");
            }
            System.out.println();
        }

        int x = 2;
        int y = 2;
        int z = x > y ? x : y;

        final int one = 1;
        switch (x) {
            default:
                System.out.println("default");
                break;
            case one:
                System.out.println("1");
            case 2:
                System.out.println("2");
        }

        String text = switch (x) {
            case one -> "one";
            case 2 -> "two";
            default -> "other";
        };

        {
            var iks = 12;
        }

        int j = 0;
        for (int k = 0; j + k != 10; j++, k++) {
            System.out.println("j=" + j + ", k=" + k);
        }

        x = 0_010_0010_0111;

        Object o = new String("some string");

        if (o instanceof String s) {
            //String s = (String) o;
            System.out.println("s=" + s.toUpperCase());
        }


        String s1 = "Hello World!";
        String s2 = new String("Hello World!");
        String s3 = "Hello World!";
        System.out.println(s1 == s2);
        System.out.println(s2 == s3);
        System.out.println(s1 == s3);

        System.out.println(1 + 2 + "trzy" + 4 + 5);


        String txt = "Hello, wonderful world!";
        StringTokenizer tokenizer = new StringTokenizer(txt);
        while (tokenizer.hasMoreTokens()) {
            System.out.println(tokenizer.nextToken());
        }



        String address = "ul. Woronicza 17, 00950 Warszawa";
        String patternText = "\\d\\d[-\\s]?\\d\\d\\d";

        Pattern pattern = Pattern.compile(patternText);
        Matcher matcher = pattern.matcher(address);

        while(matcher.find()){
            System.out.println(matcher.group() + ": " + matcher.start());
        }




    }


/*    public static int multiply(int x, int y){
        return x * y;
    }*/


}
